Adding points.
--------------

Suppose we want to add a point to the set of the available points. Let the
name of this point be "KRU" (Kruger National Park), and the location is 
24.0114S, 31.4853E

1. Extract data for the location. On a GFDL workstation or analysis machine, it
is possible to avoid this (lengthy and cumbersome) step and use the global
Sheffield forcing files, except that the model runs much slower with them,
perhaps due to slow reads from the archive. Im my test, I got a factor of 3
slow-down: from about 20 s/year to about 60 s/year.

    1. find a nearest point in the Sheffield data. Unfortunately it's not
    automated right now. Typically I open one of Sheffield forcing data in ferret
    and print a single value of `tas` (any variable would do) for a guesstimate
    of i,j. It requires just a few iterations to get the right point. For KRU, 
    j=66,i=32; coordinates of this data point are 31.5E, 24.5S
    	Sheffield data at /archive/slm/DATA/Sheffield/2011-03-14/.
   
    2. Edit point list in `/home/slm/DATA/Sheffield/2011-03-14/extract.bash`.
    There are many examples of points in this file, just follow them. Only first
    three space-separated fields are used by the script: i, j, and the short site
    name, the rest is for humans. For our point the line looks like that:
   
            32 66 KRU  Kruger National Park  actual (31.49E, 24.01S) data from (31.5E, 24.5S)

    3. Run this `extract.bash` script on analysis machine. It takes *long* time.
    Significant amount of the time is spent on copying the files to the fast disk, so if you
    plan to add several points, overall it would be much faster if you extract them all
    at once, rather than one at a time.

2. Generate grid spec for the same location. Use the coordinates of the data
point, not the original exact coordinates of the site:

        /home/slm/bin/make-1x1-gridspec -- 31.5 -24.5 /home/slm/DATA/grids/single-point/grid_spec_KRU.nc

3. Generate dummy rivers for that grid spec:

        /home/slm/bin/make-dummy-rivers /home/slm/DATA/grids/single-point/grid_spec_KRU.nc \
                                        /home/slm/DATA/land/single-point/dummy_rivers_KRU.nc

Steps 2 and 3 do not depend on data extraction, so you can do them wile the data
are being generated. After all three steps finish, you can use the point "KRU" 
in your runs.

    run -P KRU -v -t 3y -O KRU

The model run script relies on the data files being located in certain
directories. Most of these directories are owned by `slm`, and not open for
writing to everyone. The data can be moved there by request, or the model run
script can be modified to point to your own directories.
